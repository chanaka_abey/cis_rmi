package lk.radio.voting.server;

import lk.radio.voting.server.service.AdminServiceImpl;
import lk.radio.voting.server.service.PersonAnswerServiceImpl;
import lk.radio.voting.server.service.PersonServiceImpl;
import lk.radio.voting.server.service.QuestionServiceImpl;
import lk.radio.voting.server.utilities.DBUtils;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Calendar;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;


/**
 * This class use for start all the services and bind service to the registry.S
 *
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			
			DBUtils dbutils = DBUtils.getInsatnce();
			dbutils.getDbConnection();
			
			Registry registry = LocateRegistry.createRegistry(6788);
			
			AdminServiceImpl adminServiceImpl = new AdminServiceImpl();
			
			PersonServiceImpl personServiceImpl = new PersonServiceImpl();
			
			PersonAnswerServiceImpl personAnswerServiceImpl = new PersonAnswerServiceImpl();
			
			QuestionServiceImpl questionServiceImpl = new QuestionServiceImpl();
			
			registry.bind("serviceadmin", adminServiceImpl);
			
			registry.bind("servicePerson", personServiceImpl);
			
			registry.bind("servicePersonAnswer", personAnswerServiceImpl);
			
			registry.rebind("servicequestion", questionServiceImpl);
			
			System.out.print("Server is Running");
			Platform.exit();
			
//			Calendar today = Calendar.getInstance();
//			today.set(Calendar.HOUR_OF_DAY, 15);
//			today.set(Calendar.MINUTE, 34);
//			today.set(Calendar.SECOND, 0);
////			
//			Timer timer = new Timer();
//			timer.schedule(new MyTask("Winner "), today.getTime(),TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
