package lk.radio.voting.server.utilities;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author chanaka
 *
 * This class implements thread safe singleton
 */
public class DBUtils {

    private static DBUtils dbUtils;

    private DBUtils() {
    }

    public static synchronized DBUtils getInsatnce() {

        if (dbUtils == null) {
            dbUtils = new DBUtils();
        }
        return dbUtils;
    }

    public Connection getDbConnection() {

        Connection connection = null;
        String db_host = "localhost";
        String db_name = "rmi_radio_voting_system";
        String db_port = "3309";
        String db_user = "root";
        String db_password = "1234";

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUrl("jdbc:mysql://"+db_host+":"+db_port+"/"+db_name);
        System.out.println("\n"+dataSource.getURL());
        dataSource.setUser(db_user);
        dataSource.setPassword(db_password);

        try {
            dataSource.setConnectTimeout(10000);// MySQL Connection timeout 10000 ms
            connection = dataSource.getConnection();

            if (!connection.isClosed() || connection != null) {
                System.out.println("Database Connected successfully!");
            }
        } catch (SQLException ex) {
            System.out.println("Exception from : " + this.getClass().getCanonicalName());
            System.out.println("Exception is : " + ex);
        }

        return connection;
    }
}
