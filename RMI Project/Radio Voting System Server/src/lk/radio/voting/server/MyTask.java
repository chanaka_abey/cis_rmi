package lk.radio.voting.server;

import java.util.Date;
import java.util.TimerTask;

import lk.radio.voting.server.request.EmailSender;


/**
 * This class use for schedue a task.using this class we can schedule email.
 *
 */
public class MyTask extends TimerTask {
    private String name;

    public MyTask(String name) {

        this.name = name;

    }

    public void run() {

EmailSender sender = new EmailSender();
sender.SendMail();
System.out.println("Task executing :"+ this.name +" : "+new Date());
    }
}
