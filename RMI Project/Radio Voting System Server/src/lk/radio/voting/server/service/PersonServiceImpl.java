package lk.radio.voting.server.service;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lk.radio.voting.api.entity.Person;
import lk.radio.voting.api.service.PersonService;
import lk.radio.voting.server.utilities.DBUtils;


public class PersonServiceImpl extends UnicastRemoteObject implements PersonService {

	private static final long serialVersionUID = 1L;
	DBUtils dbUtils = DBUtils.getInsatnce();
	private Person person;
	
	public PersonServiceImpl() throws RemoteException {}

	@Override
	public Person insertPerson(Person person) throws RemoteException {
		
		try {
			System.out.print("\nClient " + getClientHost() + " request insertAdmin() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		PreparedStatement statement = null;
		
		String sql = "INSERT INTO person(id, user_name, nic, address, email, phone_number) VALUES (?, ?, ?, ?, ?,?)";
		
		try {
			statement =  dbUtils.getDbConnection().prepareStatement(sql);
			statement.setLong(1, person.getId());
			statement.setString(2, person.getUserName());
			statement.setString(3, person.getNic());
			statement.setString(4, person.getAddress());
			statement.setString(5, person.getEmail());
			statement.setString(6, person.getPhoneNumber());
			
			statement.execute();
			
			System.out.println("[Successfull]");
			
			return person;
			
		} catch (SQLException e) {
			System.out.println("[Failed]");
			e.printStackTrace();
			return null;
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean getPersonByNIC(String nic) throws RemoteException {
		
		boolean found = false;
		
		try {
			System.out.print("\nClient " + getClientHost() + " request gePersonByNIC() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		String databaseNIC = "";
		
		PreparedStatement statement = null;
		
		String sql = "SELECT * FROM person WHERE nic ='" + nic + "'";
		
		try {
			statement = dbUtils.getDbConnection().prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			 while (result.next()) {
				 	databaseNIC = result.getString("nic");
			    }
			 
			 if (nic.equals(databaseNIC)) {
			        System.out.println("User Is Already Exisist!\n----");
			        found = true;
			    } else {
			        System.out.println("New User, So the User can register\n----");
			        found = false;
			    }
			 result.close();
			 System.out.println("[Successfull]");
		} catch (SQLException e) {
			System.out.println("[Failed]");
			e.printStackTrace();
		}finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return found;
	}
}
