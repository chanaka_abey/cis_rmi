package lk.radio.voting.server.service;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.service.PersonAnswerService;
import lk.radio.voting.server.utilities.DBUtils;

public class PersonAnswerServiceImpl extends UnicastRemoteObject implements PersonAnswerService {

	private static final long serialVersionUID = 7269175456560515813L;
	DBUtils dbUtils = DBUtils.getInsatnce();
	
	public PersonAnswerServiceImpl() throws RemoteException {}

	@Override
	public PersonAnswer insertAnswer(PersonAnswer personAnswer) throws RemoteException {
		
		try {
			System.out.print("\nClient " + getClientHost() + " request insertAnswer() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		PreparedStatement statement = null;
		
		String sql = "INSERT INTO person_Answer(id, pid, avalue , qid) VALUES (null, ?, ?, ?)";
		
		try {
			System.out.println("Person Service Answer ");
			statement =  dbUtils.getDbConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setLong(1, personAnswer.getPid());
			statement.setString(2, personAnswer.getAvalue());
			statement.setLong(3, personAnswer.getQid());
			System.out.println("Person Service Answer ");
			System.out.println(statement);
			statement.executeUpdate();
			
			ResultSet result = statement.getGeneratedKeys();
			
			if(result.next()){
				personAnswer.setId(result.getLong(1));
			}
			
			result.close();
			System.out.println("[Successfull]");
			return personAnswer;
			
		} catch (Exception e) {
			System.out.println("[Failed]");
			e.printStackTrace();
			return null;
		}finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
