package lk.radio.voting.server.service;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.Question;
import lk.radio.voting.api.service.QuestionService;
import lk.radio.voting.server.utilities.DBUtils;

public class QuestionServiceImpl extends UnicastRemoteObject implements QuestionService {

	private static final long serialVersionUID = 2926781411029253672L;
	DBUtils dbUtils = DBUtils.getInsatnce();
	private Question question;

	public QuestionServiceImpl() throws RemoteException {}

	@Override
	public Map<Question, Answer> getAllQuestion() throws RemoteException {
		
		try {
			System.out.print("\nClient " + getClientHost() + " request getAllQuestion() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		PreparedStatement statement = null;
		
		String sql = "SELECT q.Qid,q.Quection,a.Aone,a.Atwo,a.Athree,a.Afour,a.Afive from Question q inner join answer a on q.Qid = a.Qid";
		
		try {
			statement = dbUtils.getDbConnection().prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			Map<Question,Answer> qa =  new HashMap<Question,Answer>();
			while (result.next()) {
				Question question = new Question();
				Answer answer = new Answer();
				question.setId(result.getLong("Qid"));
				question.setQuestion(result.getString("Quection"));
				answer.setAone(result.getString("Aone"));
				answer.setAtwo(result.getString("Atwo"));
				answer.setAthree(result.getString("Athree"));
				answer.setAfour(result.getString("Afour"));
				answer.setAfive(result.getString("Afive"));
				qa.put(question, answer);
			}
			result.close();
			System.out.println("[Successfull]");
			return qa;
		}
		catch (SQLException e) {
			System.out.println("[Failed]");
			e.printStackTrace();
			return null;
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}	
}
