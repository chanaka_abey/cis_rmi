package lk.radio.voting.server.service;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lk.radio.voting.api.entity.Admin;
import lk.radio.voting.api.service.AdminService;
import lk.radio.voting.server.utilities.DBUtils;

public class AdminServiceImpl extends UnicastRemoteObject implements AdminService{

	
	private static final long serialVersionUID = 1L;
	DBUtils dbUtils = DBUtils.getInsatnce();
	private Admin admin;
	
	public AdminServiceImpl() throws RemoteException {}
	
	@Override
	public Admin insertAdmin(Admin admin) throws RemoteException {

		
		try {
			System.out.print("\nClient " + getClientHost() + " request insertAdmin() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		PreparedStatement statement = null;
		
		String sql = "INSERT INTO Admin(id, user_Name, password) VALUES (null, ?, ?)";
		
		try {
			statement =  dbUtils.getDbConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, admin.getUserName());
			statement.setString(2, admin.getPassword());
			
			statement.executeUpdate();
			
			ResultSet result = statement.getGeneratedKeys();
			
			if(result.next()){
				admin.setId(result.getLong(1));
			}
			
			result.close();
			System.out.println("[Successfull]");
			return admin;
			
		} catch (SQLException e) {
			System.out.println("[Failed]");
			e.printStackTrace();
			return null;
		} finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean getAdminByUserNamePassword(String userName, String password) throws RemoteException {

		boolean found = false;
		
		try {
			System.out.print("\nClient " + getClientHost() + " request getAdminByUserNamePassword() method... ");
		} catch (ServerNotActiveException e1) {
			e1.printStackTrace();
		}
		
		String databaseUsername = "";
	    String databasePassword = "";
		
		PreparedStatement statement = null;
		
		String sql = "SELECT * FROM Admin WHERE user_name='" + userName + "' && password='" + password+ "'";
		
		try {
			statement = dbUtils.getDbConnection().prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			 while (result.next()) {
			        databaseUsername = result.getString("user_name");
			        databasePassword = result.getString("password");
			    }
			 
			 if (userName.equals(databaseUsername) && password.equals(databasePassword)) {
			        System.out.println("Successful Login!\n----");
			        found = true;
			    } else {
			        System.out.println("Incorrect User Name or Password\n----");
			        found = false;
			    }
			 result.close();
			 System.out.println("[Successfull]");
		} catch (SQLException e) {
			System.out.println("[Failed]");
			e.printStackTrace();
		}finally {
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				dbUtils.getDbConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return found;
	}
}
