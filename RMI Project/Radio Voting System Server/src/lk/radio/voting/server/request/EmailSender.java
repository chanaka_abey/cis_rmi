package lk.radio.voting.server.request;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSender {


	private static final String ST_TLS_ENABLE = "false";
    private static final String SMTP_HOST = "smtp.gmail.com";
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    private static final String FALLBACK = "false";
    private static final String SMTP_PORT = "465";
    private static final String SMTP_AUTH = "true";
    private static final String DEBUG = "false";
    private static final String STORE_PROTOCOL = "pop3";
    private static final String TRANSPORT_PROTOCOL = "smtp";
    private static final String SSL_ENABLE = "true";
    final String FromEmail = "sunethchanaka004@gmail.com";
    final String username = "chanaka";
    final String password = "0717987004";


    public Properties setEmailProperties() {

        Properties properties = System.getProperties();

        properties.put("mail.smtp.starttls.enable", ST_TLS_ENABLE);
        properties.setProperty("mail.smtp.host", SMTP_HOST);
        properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        properties.setProperty("mail.smtp.socketFactory.fallback", FALLBACK);
        properties.setProperty("mail.smtp.port", SMTP_PORT);
        properties.put("mail.smtp.auth", SMTP_AUTH);
        properties.put("mail.debug", DEBUG);
        properties.put("mail.store.protocol", STORE_PROTOCOL);
        properties.put("mail.transport.protocol", TRANSPORT_PROTOCOL);
        properties.setProperty("mail.smtp.ssl.enable", SSL_ENABLE);

        return properties;
    }

    public void SendMail() {
    	System.out.println("email");

        try {
            String emailAdd = "chanakapositive@gmail.com";

                Session sessi = Session.getInstance(setEmailProperties(),
                        new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(FromEmail, password);
                    }
                });
                sessi.setDebug(false);
                // -- Create a new message --
                Message msg = new MimeMessage(sessi);

                // -- Set the FROM and TO fields --
                msg.setFrom(new InternetAddress(FromEmail));
                msg.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(emailAdd, false));
                Date nd = new Date();
                SimpleDateFormat dt = new SimpleDateFormat("dd/MM HH:mm");

                msg.setSubject("Hiru Nam Salli Thami radio programme : " + dt.format(nd) + " Winner . ");

                //            DateFormat dat = new DateFormat("yyyy/MM/dd HH:mm:ss");
                String body = "<html>"
                        + "<head>"
                        + "<meta charset=\"UTF-8\">"
                        + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
                        + "<style>"
                        + "#response-Body{float:left}table{border-collapse:collapse}th,td{text-align:left;padding:8px}tr.row:nth-child(odd){background-color:#e5e5e5}"
                        + "</style>"
                        + "</head>"
                        + "<body>"
                        + "<div id=\"response-Body\" >"
                        + "<div id=\"heading\" style=\"color:#ce0a0a;\"><h4></h4></div>"
                        + "<div id=\"subHeading\"><p><em>\n"
                        + "</em></p></div>"
                        + "<table >"
                        + "<tr class=\"row\"><td>chamath</td></tr>"
                        + "<tr class=\"row\"><td></td></tr>"
                        + "<tr class=\"row\"><td></td></tr>"
                        + "<tr class=\"row\"><td></td></tr>"
                        + "</table>"
                        + "<br>"
                        + "<br>" + "<p>&#9758; From  :  Radio Voting App Service</p>" + "</br>" + "</br>" + "</br>" + "</br>" + "</br>" + "</br>"
                        + "<p><span><b>&#9755;   AFTER YOU GOT THIS EMAIL PLEASE CALL US : +94 71 798 7004</b></span></p>"
                        + "</div>"
                        + "</body>"
                        + "</html>";

                msg.setContent(body, "text/html; charset=utf-8");
                msg.setSentDate(new Date());
                Transport.send(msg);

            }
         catch (AddressException ex) {
            System.out.println(this.getClass().toString() + " : " + ex);

        } catch (MessagingException ex) {
            System.out.println(this.getClass().toString() + " : " + ex);

        }

    }
}