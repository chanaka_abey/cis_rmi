package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.Person;
import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.entity.Question;
import lk.radio.voting.api.service.PersonAnswerService;
import lk.radio.voting.api.service.QuestionService;

import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.RadioButton;

import static lk.radio.voting.client.RegistrationFormController.personId;
import static lk.radio.voting.client.Main.personAnswerService;
import static lk.radio.voting.client.Main.questionService;

public class QuestionnairePageOneFormController implements Initializable{

	@FXML
	private Label lblQuestionOne;
	@FXML
	private RadioButton rbtnDialog;
	@FXML
	private ToggleGroup radioGroupQuestionOne;
	@FXML
	private RadioButton rbtnMobitel;
	@FXML
	private RadioButton rbtnAirtel;
	@FXML
	private RadioButton rbtnHutch;
	@FXML
	private RadioButton rbtnOtherOne;
	@FXML
	private Label lblQuestionTwo;
	@FXML
	private RadioButton rbtnSamsung;
	@FXML
	private ToggleGroup radioGroupQuestionTwo;
	@FXML
	private RadioButton rbtnApple;
	@FXML
	private RadioButton rbtnHuawei;
	@FXML
	private RadioButton rbtnHTC;
	@FXML
	private RadioButton rbtnOtherTwo;
	@FXML
	private Button btnNext;
	
	private Main main;
	private PersonAnswerService personanswerService;
	private QuestionService quectionService;
	
	public static Map<Question, Answer> qa;
	Question firstKey;
	Question secondKey;
	
	public QuestionnairePageOneFormController(){
		quectionService = questionService;
		 personanswerService = personAnswerService;
		 try {
			qa = quectionService.getAllQuestion();
			System.out.println(qa);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//		Registry registry;
//		try {
//			registry = LocateRegistry.getRegistry("localhost", 6787);
//			personAnswerService = (PersonAnswerService) registry.lookup("servicePersonAnswer");
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NotBoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Person ID From Registration Page = " + personId);
		firstKey = (Question) qa.keySet().toArray()[0];
		Answer a1 = qa.get(firstKey);
		secondKey = (Question) qa.keySet().toArray()[1];
		Answer a2 = qa.get(secondKey);
		lblQuestionOne.setText("1 ."+firstKey.getQuestion());
		lblQuestionTwo.setText("2 ."+secondKey.getQuestion());
		rbtnDialog.setText(a1.getAone());
		rbtnMobitel.setText(a1.getAtwo());
		rbtnAirtel.setText(a1.getAthree());
		rbtnHutch.setText(a1.getAfour());
		rbtnOtherOne.setText(a1.getAfive());
		rbtnSamsung.setText(a2.getAone());
		rbtnApple.setText(a2.getAtwo());
		rbtnHuawei.setText(a2.getAthree());
		rbtnHTC.setText(a2.getAfour());
		rbtnOtherTwo.setText(a2.getAfive());
		
	}

	// Event Listener on Button[#btnNext].onAction
	@FXML
	public void onNext(ActionEvent event) {
		
		try {
			RadioButton selectedRadioButton = (RadioButton) radioGroupQuestionOne.getSelectedToggle();
			String anone = selectedRadioButton.getText();
			selectedRadioButton = (RadioButton) radioGroupQuestionTwo.getSelectedToggle();
			String antwo = selectedRadioButton.getText();
			
			System.out.println("Question One Answer = " + anone);
		    System.out.println("Question Two Answer = " + antwo);
		    

		    
		    PersonAnswer personAnswerOne = new PersonAnswer();
		    personAnswerOne.setPid(personId);
		    personAnswerOne.setAvalue(anone);
		    personAnswerOne.setQid(firstKey.getId());
		    
		    PersonAnswer personAnswerTwo = new PersonAnswer();
		    personAnswerTwo.setPid(personId);
		    personAnswerTwo.setAvalue(antwo);
		    personAnswerTwo.setQid(secondKey.getId());
		    personAnswerService.insertAnswer(personAnswerOne);
		    personAnswerService.insertAnswer(personAnswerTwo);
		    
			Parent questionPageTwoViewParent;
			questionPageTwoViewParent = FXMLLoader.load(getClass().getResource("QuestionnairePageTwoForm.fxml"));
			Scene questionPageTwoViewScene = new Scene(questionPageTwoViewParent);
			
			// This lines gets the Stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(questionPageTwoViewScene);
			window.setTitle("Question Page Two");
			window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void setMain(Main main) {
		this.main = main;
		personAnswerService = main.getPersonAnswerService();
	}
}
