package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.entity.Question;

import static lk.radio.voting.client.Main.personAnswerService;
import static lk.radio.voting.client.QuestionnairePageOneFormController.qa;
import static lk.radio.voting.client.RegistrationFormController.personId;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.RadioButton;

public class QuestionnairePageFourFormController implements Initializable {
	@FXML
	private Label lblQuestionSeven;
	@FXML
	private RadioButton rbtnRock;
	@FXML
	private ToggleGroup radioGroupQuestionSeven;
	@FXML
	private RadioButton rbtnMetal;
	@FXML
	private RadioButton rbtnPop;
	@FXML
	private RadioButton rbtnClassical;
	@FXML
	private RadioButton rbtnOtherOne;
	@FXML
	private Label lblQuestionEight;
	@FXML
	private RadioButton rbtnSportProgram;
	@FXML
	private ToggleGroup radioGroupQuestionEight;
	@FXML
	private RadioButton rbtnNewsProgram;
	@FXML
	private RadioButton rbtnMusicalProgram;
	@FXML
	private RadioButton rbtnPoliticalProgram;
	@FXML
	private RadioButton rbtnOtherTwo;
	@FXML
	private Button btnNext;
	
	Question seventhkey;
	Question eightthkey;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Person ID Pass From Question Page Three = " + personId);
		seventhkey = (Question) qa.keySet().toArray()[6];
		Answer a7 = qa.get(seventhkey);
		eightthkey = (Question) qa.keySet().toArray()[7];
		Answer a8 = qa.get(eightthkey);
		lblQuestionSeven.setText("7 ."+seventhkey.getQuestion());
		lblQuestionEight.setText("8 ."+eightthkey.getQuestion());
		rbtnRock.setText(a7.getAone());
		rbtnMetal.setText(a7.getAtwo());
		rbtnPop.setText(a7.getAthree());
		rbtnClassical.setText(a7.getAfour());
		rbtnOtherOne.setText(a7.getAfive());
		rbtnSportProgram.setText(a8.getAone());
		rbtnNewsProgram.setText(a8.getAtwo());
		rbtnMusicalProgram.setText(a8.getAthree());
		rbtnPoliticalProgram.setText(a8.getAfour());
		rbtnOtherTwo.setText(a8.getAfive());
	}

	// Event Listener on Button[#btnNext].onAction
	@FXML
	public void onNext(ActionEvent event) {
		try {
			
			RadioButton selectedRadioButton = (RadioButton) radioGroupQuestionSeven.getSelectedToggle();
			String aseven = selectedRadioButton.getText();
			selectedRadioButton = (RadioButton) radioGroupQuestionSeven.getSelectedToggle();
			String aeight = selectedRadioButton.getText();
			
			System.out.println("Question Seven Answer = " + aseven);
		    System.out.println("Question Eight Answer = " + aeight);
		    
		    PersonAnswer personAnswerSeven = new PersonAnswer();
		    personAnswerSeven.setPid(personId);
		    personAnswerSeven.setAvalue(aseven);
		    personAnswerSeven.setQid(seventhkey.getId());
		    
		    PersonAnswer personAnswerEight = new PersonAnswer();
		    personAnswerEight.setPid(personId);
		    personAnswerEight.setAvalue(aeight);
		    personAnswerEight.setQid(eightthkey.getId());
		    
		    personAnswerService.insertAnswer(personAnswerSeven);
		    personAnswerService.insertAnswer(personAnswerEight);
		    
			Parent questionPageFourViewParent;
			questionPageFourViewParent = FXMLLoader.load(getClass().getResource("QuestionnairePageFiveForm.fxml"));
			Scene questionPageFourViewScene = new Scene(questionPageFourViewParent);
			
			// This lines gets the Stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(questionPageFourViewScene);
			window.setTitle("Question Page Five");
			window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setMain(Main main) {
		personAnswerService = main.getPersonAnswerService();
	}
}
