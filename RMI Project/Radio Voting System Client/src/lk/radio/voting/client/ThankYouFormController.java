package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;

public class ThankYouFormController {
	@FXML
	private JFXButton btnNewUser;

	// Event Listener on JFXButton[#btnNewUser].onAction
	@FXML
	public void onClick(ActionEvent event) {
		
		try {
			
			// get a handle to the stage
		    Stage stage = (Stage) btnNewUser.getScene().getWindow();
		    // do what you have to do
		    stage.close();
		    
		    
		    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RegistrationForm.fxml"));
		    
		    Parent root = fxmlLoader.load();
		    
		    Stage stageo = new Stage();
		    RegistrationFormController controller = fxmlLoader.getController();
			
			controller.setMain(Main.runner);
			
			stageo.setScene(new Scene(root));
			
			stageo.setTitle("Registration Form");
			
			stageo.show();
		    
			
//			Parent registrationPageViewParent;
//			registrationPageViewParent = FXMLLoader.load(getClass().getResource("RegistrationForm.fxml"));
//			Scene registrationPageViewScene = new Scene(registrationPageViewParent);
//			
//			// This lines gets the Stage information
//			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
//			
//			window.setScene(registrationPageViewScene);
//			window.setTitle("Registration Page");
//			window.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
