package lk.radio.voting.client;
	
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Person;
import lk.radio.voting.api.service.PersonAnswerService;
import lk.radio.voting.api.service.PersonService;
import lk.radio.voting.api.service.QuestionService;

public class Main extends Application {
	
	private PersonService personService;
	public static PersonAnswerService personAnswerService;
	public static QuestionService questionService;
	public static Stage stg;
	public static Main runner;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			this.stg = primaryStage;
			System.out.print("Normal Client");
			
			Registry registry = LocateRegistry.getRegistry("localhost", 6788);
			
			personService = (PersonService) registry.lookup("servicePerson");
			personAnswerService = (PersonAnswerService) registry.lookup("servicePersonAnswer");
			questionService = (QuestionService) registry.lookup("servicequestion");
			
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RegistrationForm.fxml"));
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TestSetValuesForm.fxml"));
			
			Parent root = fxmlLoader.load();
			
			RegistrationFormController controller = fxmlLoader.getController();
			
			runner = this;
			controller.setMain(runner);
			
			
			stg.setScene(new Scene(root));
			
			stg.setTitle("Registration Form");
			
			stg.show();
			
			// API Implementation for get the IP
		    try (java.util.Scanner s = new java.util.Scanner(new java.net.URL("https://api.ipify.org").openStream(), "UTF-8").useDelimiter("\\A")) {
	            System.out.println("\nUser Client current IP address is " + s.next());
	        } catch (java.io.IOException e) {
	            e.printStackTrace();
	        }
			
//			Person p = new Person();
//			p.setUserName("Person Ten");
//			p.setNic("122334567V");
//			p.setAddress("Test address");
//			p.setEmail("test@gmail.com");
//			p.setPhoneNumber("0112343233");
			
//			p = personService.insertPerson(p);
//			
//			System.out.println(p.getId());
//			System.out.println(p.getUserName());
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public PersonService getPersonService() {
		return personService;
	}
	
	public PersonAnswerService getPersonAnswerService() {
		return personAnswerService;
	}
	
	public QuestionService getQuestionService(){
		return questionService;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
