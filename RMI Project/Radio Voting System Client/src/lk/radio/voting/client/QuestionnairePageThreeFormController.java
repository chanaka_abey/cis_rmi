package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.entity.Question;

import static lk.radio.voting.client.Main.personAnswerService;
import static lk.radio.voting.client.QuestionnairePageOneFormController.qa;
import static lk.radio.voting.client.RegistrationFormController.personId;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.RadioButton;

public class QuestionnairePageThreeFormController implements Initializable {
	@FXML
	private Label lblQuestionFive;
	@FXML
	private RadioButton rbtnHiru;
	@FXML
	private ToggleGroup radioGroupQuestionFive;
	@FXML
	private RadioButton rbtnSirasa;
	@FXML
	private RadioButton rbtnDerana;
	@FXML
	private RadioButton rbtnNeth;
	@FXML
	private RadioButton rbtnOther;
	@FXML
	private Label lblQuestionSix;
	@FXML
	private RadioButton rbtnEarlyMorning;
	@FXML
	private ToggleGroup radioGroupQuestionSix;
	@FXML
	private RadioButton rbtnLateMorning;
	@FXML
	private RadioButton rbtnAfternoon;
	@FXML
	private RadioButton rbtnEvening;
	@FXML
	private RadioButton rbtnLateNightEarlyHours;
	@FXML
	private Button btnNext;
	
	Question fifthkey;
	Question sixthkey;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Person ID Pass From Question Page Two = " + personId);
		fifthkey = (Question) qa.keySet().toArray()[4];
		Answer a5 = qa.get(fifthkey);
		sixthkey = (Question) qa.keySet().toArray()[5];
		Answer a6 = qa.get(sixthkey);
		lblQuestionFive.setText("5 ."+fifthkey.getQuestion());
		lblQuestionFive.setText("6 ."+sixthkey.getQuestion());
		rbtnHiru.setText(a5.getAone());
		rbtnSirasa.setText(a5.getAtwo());
		rbtnDerana.setText(a5.getAthree());
		rbtnNeth.setText(a5.getAfour());
		rbtnOther.setText(a5.getAfive());
		rbtnEarlyMorning.setText(a6.getAone());
		rbtnLateMorning.setText(a6.getAtwo());
		rbtnAfternoon.setText(a6.getAthree());
		rbtnEvening.setText(a6.getAfour());
		rbtnLateNightEarlyHours.setText(a6.getAfive());
	}

	// Event Listener on Button[#btnNext].onAction
	@FXML
	public void onNext(ActionEvent event) {
		try {
			RadioButton selectedRadioButton = (RadioButton) radioGroupQuestionFive.getSelectedToggle();
			String afive = selectedRadioButton.getText();
			selectedRadioButton = (RadioButton) radioGroupQuestionSix.getSelectedToggle();
			String asix = selectedRadioButton.getText();
			
			System.out.println("Question Five Answer = " + afive);
		    System.out.println("Question Six Answer = " + asix);
		    
		    PersonAnswer personAnswerFive = new PersonAnswer();
		    personAnswerFive.setPid(personId);
		    personAnswerFive.setAvalue(afive);
		    personAnswerFive.setQid(fifthkey.getId());
		    
		    PersonAnswer personAnswerSix = new PersonAnswer();
		    personAnswerSix.setPid(personId);
		    personAnswerSix.setAvalue(asix);
		    personAnswerSix.setQid(sixthkey.getId());

		    personAnswerService.insertAnswer(personAnswerFive);
		    personAnswerService.insertAnswer(personAnswerSix);
		    
			Parent questionPageFourViewParent;
			questionPageFourViewParent = FXMLLoader.load(getClass().getResource("QuestionnairePageFourForm.fxml"));
			Scene questionPageFourViewScene = new Scene(questionPageFourViewParent);
			
			// This lines gets the Stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(questionPageFourViewScene);
			window.setTitle("Question Page Four");
			window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setMain(Main main) {
		personAnswerService = main.getPersonAnswerService();
	}
}
