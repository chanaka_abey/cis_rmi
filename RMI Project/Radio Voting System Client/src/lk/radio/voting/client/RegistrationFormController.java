package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Person;
import lk.radio.voting.api.service.PersonService;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

public class RegistrationFormController implements Initializable{
	@FXML
	private TextField txtUserName;
	@FXML
	private TextField txtNic;
	@FXML
	private TextField txtAddress;
	@FXML
	private TextField txtEmail;
	@FXML
	private TextField txtPhoneNumber;
	@FXML
	private Button btnReset;
	@FXML
	private Button btnContinue;
	@FXML
	private Label lblid;
	
	private Main main;
	private PersonService personService;
	public static Long personId;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		long uniqueID = (System.currentTimeMillis() & 0xfffffff);
		lblid.setText(String.valueOf(uniqueID));
	}

	// Event Listener on Button[#btnReset].onAction
	@FXML
	public void onReset(ActionEvent event) {
		txtUserName.setText("");
		txtNic.setText("");
		txtAddress.setText("");
		txtEmail.setText("");
		txtPhoneNumber.setText("");
	}
	
	// Event Listener on Button[#btnContinue].onAction
	@FXML
	public void onContinue(ActionEvent event) {
		String nicNo = txtNic.getText();
		boolean found = false;
		
		if(isFieldValid()){
			try {
				found = personService.getPersonByNIC(nicNo);
				
				if(found == true){
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("User Is Already Exist");
					alert.setContentText("Ooops, try with different NIC Number!");
					alert.showAndWait();
				}
				else {
					Person person = new Person();
					person.setId(Long.parseLong(lblid.getText()));
					person.setUserName(txtUserName.getText());
					person.setNic(txtNic.getText());
					person.setAddress(txtAddress.getText());
					person.setEmail(txtEmail.getText());
					person.setPhoneNumber(txtPhoneNumber.getText());
					
					personService.insertPerson(person);
					
					personId = person.getId();
					System.out.println("\nRegistration Form Genarated Person Id = " + personId);
					
					clearField();
				
					Parent questionPageOneViewParent;
					questionPageOneViewParent = FXMLLoader.load(getClass().getResource("QuestionnairePageOneForm.fxml"));
					Scene questionPageOneViewScene = new Scene(questionPageOneViewParent);
					
					// This lines gets the Stage information
					Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
					
					window.setScene(questionPageOneViewScene);
					window.setTitle("Question Page One");
					window.show();
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	// Validate the Fields
	private boolean isFieldValid(){

		String errorMessage = "";
		String email = txtEmail.getText();
		String phoneNumber = txtPhoneNumber.getText();
		String nicNumber = txtNic.getText();
		String regexEmail = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		String regexPhoneNumber = "\\A[0-9]{10}\\z";
		String regexnicNumber = "[0-9]{9}[x|X|v|V]$";
		
		if(txtUserName.getText() == null || txtUserName.getText().isEmpty()){
			errorMessage += "User Name Field is Empty!\n";
		}
		
		if(txtNic.getText() == null || txtNic.getText().isEmpty()){
			errorMessage += "National Identity Card Number Field is Empty!\n";
		}
		if(txtAddress.getText() == null || txtAddress.getText().isEmpty()){
			errorMessage += "Address Field is Empty!\n";
		}
		if(txtEmail.getText() == null || txtEmail.getText().isEmpty()){
			errorMessage += "Email Field is Empty!\n";
		}
		if(txtPhoneNumber.getText() == null || txtPhoneNumber.getText().isEmpty()){
			errorMessage += "Phone Number Field is Empty!\n";
		}
		if(email.matches(regexEmail) == false){
			errorMessage += "Invalid Email Address!\n";
		}
		if(nicNumber.matches(regexnicNumber)== false){
			errorMessage += "Invalid National Identity Card Number!\n";
		}
		if(phoneNumber.matches(regexPhoneNumber)==false){
			errorMessage += "Invalid Phone Number!\n";
		}
		if(errorMessage.length()== 0){
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.setTitle("Error");
			alert.setHeaderText("Please Corret invalid fields");
			alert.setContentText(errorMessage);
			alert.showAndWait();
			return false;
		}
	}
	
	private void clearField() {
		txtUserName.setText("");
		txtNic.setText("");
		txtAddress.setText("");
		txtEmail.setText("");
		txtPhoneNumber.setText("");
	}
	
	public void setMain(Main main) {
		this.main = main;
		this.personService = main.getPersonService();
	}
}
