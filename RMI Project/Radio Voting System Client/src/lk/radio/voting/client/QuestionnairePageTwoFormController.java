package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.entity.Question;
import lk.radio.voting.api.service.PersonAnswerService;
import lk.radio.voting.api.service.QuestionService;

import static lk.radio.voting.client.Main.personAnswerService;
import static lk.radio.voting.client.Main.questionService;
import static lk.radio.voting.client.RegistrationFormController.personId;
import static lk.radio.voting.client.QuestionnairePageOneFormController.qa;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.RadioButton;

public class QuestionnairePageTwoFormController implements Initializable {
	@FXML
	private Label lblQuestionThree;
	@FXML
	private RadioButton rbtnCricket;
	@FXML
	private ToggleGroup radioGroupQuestionThree;
	@FXML
	private RadioButton rbtnFootball;
	@FXML
	private RadioButton rbtnVolleyball;
	@FXML
	private RadioButton rbtnBadminton;
	@FXML
	private RadioButton rbtnOtherOne;
	@FXML
	private Label lblQuestionFour;
	@FXML
	private RadioButton rbtnNovelBook;
	@FXML
	private ToggleGroup radioGroupQuestionFour;
	@FXML
	private RadioButton rbtnShortStoryBook;
	@FXML
	private RadioButton rbtnFairyTails;
	@FXML
	private RadioButton rbtnMotivationBook;
	@FXML
	private RadioButton rbtnOtherTwo;
	@FXML
	private Button btnNext;
	
	Question thirdKey;
	Question fourthKey;
	public QuestionnairePageTwoFormController() {
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Person ID Pass From Question Page One = " + qa);
		thirdKey = (Question) qa.keySet().toArray()[2];
		Answer a3 = qa.get(thirdKey);
		fourthKey = (Question) qa.keySet().toArray()[3];
		Answer a4 = qa.get(fourthKey);
		lblQuestionThree.setText("3 ."+thirdKey.getQuestion());
		lblQuestionFour.setText("4 ."+fourthKey.getQuestion());
		rbtnCricket.setText(a3.getAone());
		rbtnFootball.setText(a3.getAtwo());
		rbtnVolleyball.setText(a3.getAthree());
		rbtnBadminton.setText(a3.getAfour());
		rbtnOtherOne.setText(a3.getAfive());
		rbtnNovelBook.setText(a4.getAone());
		rbtnShortStoryBook.setText(a4.getAtwo());
		rbtnFairyTails.setText(a4.getAthree());
		rbtnMotivationBook.setText(a4.getAfour());
		rbtnOtherTwo.setText(a4.getAfive());	
	}

	// Event Listener on Button[#btnNext].onAction
	@FXML
	public void onNext(ActionEvent event) {
		
		try {
			RadioButton selectedRadioButton = (RadioButton) radioGroupQuestionThree.getSelectedToggle();
			String athree = selectedRadioButton.getText();
			selectedRadioButton = (RadioButton) radioGroupQuestionFour.getSelectedToggle();
			String anfour= selectedRadioButton.getText();
			
			System.out.println("Question Three Answer = " + athree);
		    System.out.println("Question Four Answer = " + anfour);
		    
		    PersonAnswer personAnswerThree = new PersonAnswer();
		    personAnswerThree.setPid(personId);
		    personAnswerThree.setAvalue(athree);
		    personAnswerThree.setQid(thirdKey.getId());
		    
		    PersonAnswer personAnswerFour = new PersonAnswer();
		    personAnswerFour.setPid(personId);
		    personAnswerFour.setAvalue(anfour);
		    personAnswerFour.setQid(fourthKey.getId());

		    personAnswerService.insertAnswer(personAnswerThree);
		    personAnswerService.insertAnswer(personAnswerFour);
		    
			Parent questionPageThreeViewParent;
			questionPageThreeViewParent = FXMLLoader.load(getClass().getResource("QuestionnairePageThreeForm.fxml"));
			Scene questionPageThreeViewScene = new Scene(questionPageThreeViewParent);
			
			// This lines gets the Stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(questionPageThreeViewScene);
			window.setTitle("Question Page Three");
			window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setMain(Main main) {
		personAnswerService = main.getPersonAnswerService();
	}
}
