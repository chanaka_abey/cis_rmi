package lk.radio.voting.client;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.PersonAnswer;
import lk.radio.voting.api.entity.Question;

import static lk.radio.voting.client.Main.personAnswerService;
import static lk.radio.voting.client.QuestionnairePageOneFormController.qa;
import static lk.radio.voting.client.RegistrationFormController.personId;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.RadioButton;

public class QuestionnairePageFiveFormController implements Initializable {
	@FXML
	private Label lblQuestionNine;
	@FXML
	private RadioButton rbtnOneHourOrBelow;
	@FXML
	private ToggleGroup radioGroupQuestionNine;
	@FXML
	private RadioButton rbtnOneHourToTwoHour;
	@FXML
	private RadioButton rbtnOneHourToFiveHour;
	@FXML
	private RadioButton rbtnFiveHourToTenHour;
	@FXML
	private RadioButton rbtnMoreThanTenHour;
	@FXML
	private Label lblQuestionTen;
	@FXML
	private RadioButton rbtnSriLanka;
	@FXML
	private ToggleGroup radioGroupQuestionTen;
	@FXML
	private RadioButton rbtnSingapore;
	@FXML
	private RadioButton rbtnMalasia;
	@FXML
	private RadioButton rbtnUSA;
	@FXML
	private RadioButton rbtnNewZeland;
	@FXML
	private Button btnDone;
	
	Question ninethkey;
	Question tenthkey;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Person ID Pass From Question Page Four = " + personId);
		ninethkey = (Question) qa.keySet().toArray()[8];
		Answer a9 = qa.get(ninethkey);
		tenthkey = (Question) qa.keySet().toArray()[9];
		Answer a10 = qa.get(tenthkey);
		lblQuestionNine.setText("9 ."+ninethkey.getQuestion());
		lblQuestionTen.setText("10 ."+tenthkey.getQuestion());
		rbtnOneHourOrBelow.setText(a9.getAone());
		rbtnOneHourToTwoHour.setText(a9.getAtwo());
		rbtnOneHourToFiveHour.setText(a9.getAthree());
		rbtnFiveHourToTenHour.setText(a9.getAfour());
		rbtnMoreThanTenHour.setText(a9.getAfive());
		rbtnSriLanka.setText(a10.getAone());
		rbtnSingapore.setText(a10.getAtwo());
		rbtnMalasia.setText(a10.getAthree());
		rbtnUSA.setText(a10.getAfour());
		rbtnNewZeland.setText(a10.getAfive());
	}

	// Event Listener on Button[#btnDone].onAction
	@FXML
	public void onDone(ActionEvent event) {
		try {
			RadioButton selectedRadioButton = (RadioButton) radioGroupQuestionNine.getSelectedToggle();
			String anine = selectedRadioButton.getText();
			selectedRadioButton = (RadioButton) radioGroupQuestionTen.getSelectedToggle();
			String aten = selectedRadioButton.getText();
			
			System.out.println("Question Nine Answer = " + anine);
		    System.out.println("Question Ten Answer = " + aten);
		    
		    PersonAnswer personAnswerNine = new PersonAnswer();
		    personAnswerNine.setPid(personId);
		    personAnswerNine.setAvalue(anine);
		    personAnswerNine.setQid(ninethkey.getId());
		    
		    PersonAnswer personAnswerTen = new PersonAnswer();
		    personAnswerTen.setPid(personId);
		    personAnswerTen.setAvalue(aten);
		    personAnswerTen.setQid(tenthkey.getId());
		    
		    personAnswerService.insertAnswer(personAnswerNine);
		    personAnswerService.insertAnswer(personAnswerTen);
		    
		    Parent questionPageThankyouViewParent;
		    questionPageThankyouViewParent = FXMLLoader.load(getClass().getResource("ThankYouForm.fxml"));
			Scene questionPageThankyouViewScene = new Scene(questionPageThankyouViewParent);
			
			// This lines gets the Stage information
			Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
			
			window.setScene(questionPageThankyouViewScene);
			window.setTitle("Thank You Page");
			window.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setMain(Main main) {
		personAnswerService = main.getPersonAnswerService();
	}
}
