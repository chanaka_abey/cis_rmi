package lk.radio.voting.api.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonAnswer implements Externalizable {
	
	private final LongProperty id = new SimpleLongProperty();
	private final LongProperty pid = new SimpleLongProperty();
	private final StringProperty avalue = new SimpleStringProperty();
	private final LongProperty qid = new SimpleLongProperty();

	public LongProperty idProperty() {
		return this.id;
	}
	

	public long getId() {
		return this.idProperty().get();
	}
	

	public void setId(final long id) {
		this.idProperty().set(id);
	}
	

	public LongProperty pidProperty() {
		return this.pid;
	}
	

	public long getPid() {
		return this.pidProperty().get();
	}
	

	public void setPid(final long pid) {
		this.pidProperty().set(pid);
	}
	
	public final StringProperty avalueProperty() {
		return this.avalue;
	}
	


	public final String getAvalue() {
		return this.avalueProperty().get();
	}
	


	public final void setAvalue(final String avalue) {
		this.avalueProperty().set(avalue);
	}
	
	
	public LongProperty qidProperty() {
		return this.qid;
	}
	
	public long getQid() {
		return this.qidProperty().get();
	}
	

	public void setQid(final long qid) {
		this.qidProperty().set(qid);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(getId());
		out.writeLong(getPid());
		out.writeObject(getAvalue());
		out.writeLong(getQid());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		setId(in.readLong());
		setPid(in.readLong());
		setAvalue((String) in.readObject());
		setQid(in.readLong());
	}


	
	



	
}
