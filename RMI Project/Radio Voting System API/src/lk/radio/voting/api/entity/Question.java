package lk.radio.voting.api.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Question implements Externalizable{
	
	private final LongProperty id  = new SimpleLongProperty();
	private final StringProperty question  = new SimpleStringProperty();
	public LongProperty idProperty() {
		return this.id;
	}
	
	public long getId() {
		return this.idProperty().get();
	}
	
	public void setId(final long id) {
		this.idProperty().set(id);
	}
	
	public StringProperty questionProperty() {
		return this.question;
	}
	
	public String getQuestion() {
		return this.questionProperty().get();
	}
	
	public void setQuestion(final String question) {
		this.questionProperty().set(question);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(getId());
		out.writeObject(getQuestion());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		setId(in.readLong());
		setQuestion((String) in.readObject());
	}
	

}
