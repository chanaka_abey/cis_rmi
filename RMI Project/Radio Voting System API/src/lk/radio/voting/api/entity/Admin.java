package lk.radio.voting.api.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Admin implements Externalizable {
	
	private final LongProperty id  = new SimpleLongProperty();
	private final StringProperty userName  = new SimpleStringProperty();
	private final StringProperty password  = new SimpleStringProperty();
	
	public LongProperty idProperty() {
		return this.id;
	}
	
	public long getId() {
		return this.idProperty().get();
	}
	
	public void setId(final long id) {
		this.idProperty().set(id);
	}
	
	public StringProperty userNameProperty() {
		return this.userName;
	}
	
	public String getUserName() {
		return this.userNameProperty().get();
	}
	
	public void setUserName(final String userName) {
		this.userNameProperty().set(userName);
	}
	
	public StringProperty passwordProperty() {
		return this.password;
	}
	
	public String getPassword() {
		return this.passwordProperty().get();
	}
	
	public void setPassword(final String password) {
		this.passwordProperty().set(password);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(getId());
		out.writeObject(getUserName());
		out.writeObject(getPassword());	
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		setId(in.readLong());
		setUserName((String) in.readObject());
		setPassword((String) in.readObject());
	}
	
}
