package lk.radio.voting.api.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Answer implements Externalizable{

	private final LongProperty qid = new SimpleLongProperty();
	private final StringProperty aone = new SimpleStringProperty();
	private final StringProperty atwo = new SimpleStringProperty();
	private final StringProperty athree = new SimpleStringProperty();
	private final StringProperty afour = new SimpleStringProperty();
	private final StringProperty afive = new SimpleStringProperty();

	@Override
	public void readExternal(ObjectInput arg0) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		setQid(arg0.readLong());
		setAone((String) arg0.readObject());
		setAtwo((String) arg0.readObject());
		setAthree((String) arg0.readObject());
		setAfour((String) arg0.readObject());
		setAfive((String) arg0.readObject());
		
	}
	@Override
	public void writeExternal(ObjectOutput arg0) throws IOException {
		arg0.writeLong(getQid());
		arg0.writeObject(getAone());
		arg0.writeObject(getAtwo());
		arg0.writeObject(getAthree());
		arg0.writeObject(getAfour());
		arg0.writeObject(getAfive());
		// TODO Auto-generated method stub
		
	}
	
	public final LongProperty qidProperty() {
		return this.qid;
	}
	
	public final long getQid() {
		return this.qidProperty().get();
	}
	
	public final void setQid(final long qid) {
		this.qidProperty().set(qid);
	}
	
	public final StringProperty aoneProperty() {
		return this.aone;
	}
	
	public final String getAone() {
		return this.aoneProperty().get();
	}
	
	public final void setAone(final String aone) {
		this.aoneProperty().set(aone);
	}
	
	public final StringProperty atwoProperty() {
		return this.atwo;
	}
	
	public final String getAtwo() {
		return this.atwoProperty().get();
	}
	
	public final void setAtwo(final String atwo) {
		this.atwoProperty().set(atwo);
	}
	
	public final StringProperty athreeProperty() {
		return this.athree;
	}
	
	public final String getAthree() {
		return this.athreeProperty().get();
	}
	
	public final void setAthree(final String athree) {
		this.athreeProperty().set(athree);
	}
	
	public final StringProperty afourProperty() {
		return this.afour;
	}
	
	public final String getAfour() {
		return this.afourProperty().get();
	}
	
	public final void setAfour(final String afour) {
		this.afourProperty().set(afour);
	}
	
	public final StringProperty afiveProperty() {
		return this.afive;
	}
	
	public final String getAfive() {
		return this.afiveProperty().get();
	}
	
	public final void setAfive(final String afive) {
		this.afiveProperty().set(afive);
	}
	
}
