package lk.radio.voting.api.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person implements Externalizable{
	
	private final LongProperty id = new SimpleLongProperty();
	private final StringProperty userName = new SimpleStringProperty();
	private final StringProperty nic = new SimpleStringProperty();
	private final StringProperty address = new SimpleStringProperty();
	private final StringProperty email = new SimpleStringProperty();
	private final StringProperty phoneNumber = new SimpleStringProperty();

	public LongProperty idProperty() {
		return this.id;
	}
	

	public long getId() {
		return this.idProperty().get();
	}
	

	public void setId(final long id) {
		this.idProperty().set(id);
	}
	

	public StringProperty userNameProperty() {
		return this.userName;
	}
	

	public String getUserName() {
		return this.userNameProperty().get();
	}
	

	public void setUserName(final String userName) {
		this.userNameProperty().set(userName);
	}
	

	public StringProperty nicProperty() {
		return this.nic;
	}
	

	public String getNic() {
		return this.nicProperty().get();
	}
	

	public void setNic(final String nic) {
		this.nicProperty().set(nic);
	}
	

	public StringProperty addressProperty() {
		return this.address;
	}
	

	public String getAddress() {
		return this.addressProperty().get();
	}
	

	public void setAddress(final String address) {
		this.addressProperty().set(address);
	}
	

	public StringProperty emailProperty() {
		return this.email;
	}
	

	public String getEmail() {
		return this.emailProperty().get();
	}
	

	public void setEmail(final String email) {
		this.emailProperty().set(email);
	}
	

	public StringProperty phoneNumberProperty() {
		return this.phoneNumber;
	}
	

	public String getPhoneNumber() {
		return this.phoneNumberProperty().get();
	}
	

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumberProperty().set(phoneNumber);
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(getId());
		out.writeObject(getUserName());
		out.writeObject(getNic());
		out.writeObject(getAddress());
		out.writeObject(getEmail());
		out.writeObject(getPhoneNumber());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		setId(in.readLong());
		setUserName((String) in.readObject());
		setNic((String) in.readObject());
		setAddress((String) in.readObject());
		setEmail((String) in.readObject());
		setPhoneNumber((String) in.readObject());
	}

}
