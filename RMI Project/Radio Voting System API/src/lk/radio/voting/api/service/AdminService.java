package lk.radio.voting.api.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

import lk.radio.voting.api.entity.Admin;

public interface AdminService extends Remote {
	
	Admin insertAdmin(Admin admin) throws RemoteException;
	
	public boolean getAdminByUserNamePassword (String userName, String password) throws RemoteException;
}
