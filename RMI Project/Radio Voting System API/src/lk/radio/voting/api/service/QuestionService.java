package lk.radio.voting.api.service;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lk.radio.voting.api.entity.Answer;
import lk.radio.voting.api.entity.Question;

public interface QuestionService extends Remote {

	// Get All Questions to List
	Map<Question, Answer> getAllQuestion() throws RemoteException;
}
