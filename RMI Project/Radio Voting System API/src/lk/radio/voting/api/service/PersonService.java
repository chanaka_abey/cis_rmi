package lk.radio.voting.api.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

import lk.radio.voting.api.entity.Person;


public interface PersonService extends Remote {

	Person insertPerson(Person person) throws RemoteException;
	
	public boolean getPersonByNIC(String nic) throws RemoteException;

}
