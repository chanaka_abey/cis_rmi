package lk.radio.voting.api.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

import lk.radio.voting.api.entity.PersonAnswer;

public interface PersonAnswerService extends Remote {

	PersonAnswer insertAnswer(PersonAnswer personAnswer) throws RemoteException;
	
}
