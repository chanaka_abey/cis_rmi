package lk.radio.voting.clientadmin;
	
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lk.radio.voting.api.service.AdminService;


public class Main extends Application  {
	
	private AdminService adminService;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			System.out.print("Admin Client");
			
			Registry registry = LocateRegistry.getRegistry("localhost", 6788);
			
			adminService = (AdminService) registry.lookup("serviceadmin");
			
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LoginForm.fxml"));
			
			Parent root = fxmlLoader.load();
			
			LoginFormController controller = fxmlLoader.getController();
			
			controller.setMain(this);
			
			primaryStage.setScene(new Scene(root));
			
			primaryStage.setTitle("Login Form");
			
			primaryStage.show();
			
			// API Implementation for get the IP
		    try (java.util.Scanner s = new java.util.Scanner(new java.net.URL("https://api.ipify.org").openStream(), "UTF-8").useDelimiter("\\A")) {
	            System.out.println("\n Admin Client current IP address is " + s.next());
	        } catch (java.io.IOException e) {
	            e.printStackTrace();
	        }
			
//			Admin a = new Admin();
//			a.setUserName("adminFive");
//			a.setPasswod("adminFive");
//			
//			a = adminService.insertAdmin(a);
//			
//			System.out.println(a.getId());
//			System.out.println(a.getUserName());
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public AdminService getAdminService(){
		return adminService;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
