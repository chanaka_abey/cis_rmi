package lk.radio.voting.clientadmin;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lk.radio.voting.api.service.AdminService;

import java.io.IOException;
import java.rmi.RemoteException;

import javafx.event.ActionEvent;

import javafx.scene.control.PasswordField;

public class LoginFormController {
	@FXML
	private TextField txtUserName;
	@FXML
	private Button btnLogin;
	@FXML
	private Button btnCancel;
	@FXML
	private PasswordField txtPassword;
	
	private Main main;
	private AdminService adminService;

	// Event Listener on Button[#btnLogin].onAction
	@FXML
	public void onLogin(ActionEvent event) {
		String userName = txtUserName.getText();
		String password = txtPassword.getText();
		boolean found = false;
		
		if(isFieldValid()){
			try {
				found = adminService.getAdminByUserNamePassword(userName, password);
				
				clearField();
				
				if(found == true){
					Parent dashboardViewParent = FXMLLoader.load(getClass().getResource("Dashboard.fxml"));
					Scene dashboardViewScene = new Scene(dashboardViewParent);           
					
					// This lines gets the Stage information
					Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
					
					window.setScene(dashboardViewScene);
					window.setTitle("Dashboard Form");
					window.show();
				}
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText("Invalid Credentials");
					alert.setContentText("Ooops, there was an error login!");
					alert.showAndWait();
				}
				
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
	}
	// Event Listener on Button[#btnCancel].onAction
	@FXML
	public void onCancel(ActionEvent event) {
		// get a handle to the stage
	    Stage stage = (Stage) btnCancel.getScene().getWindow();
	    // do what you have to do
	    stage.close();
	}
	
	public void setMain(Main main) {
		this.main = main;
		this.adminService = main.getAdminService();
	}
	
	private boolean isFieldValid() {
		String errorMessage = "";
		
		if(txtUserName.getText() == null || txtUserName.getText().isEmpty()){
			errorMessage += "User Name Field is Empty!\n";
		}
		
		if(txtPassword.getText() == null || txtPassword.getText().isEmpty()){
			errorMessage += "Password Field is Empty!\n";
		}
		
		if(errorMessage.length()== 0){
			return true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
			alert.initModality(Modality.APPLICATION_MODAL);
			alert.setTitle("Error");
			alert.setHeaderText("Please Corret invalid fields");
			alert.setContentText(errorMessage);
			alert.showAndWait();
			return false;
		}
	}
	
	private void clearField() {
		txtUserName.setText("");
		txtPassword.setText("");
	}
}
